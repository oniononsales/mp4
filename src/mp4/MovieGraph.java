package mp4;

import java.io.IOException;
import java.util.*;

public class MovieGraph {
	// REP INVARIANT: Keys will always be movies, values will be a list of edges
	// REP INVARIANT: Keys and values will always be non-null
	// REP INVARIANT: MovieGraphID will always be 37
	// ABSTRACTION FUNCTION: represents an undirected graph of movies and edges
	// between movies
	// our movie map
	private final Map<Movie, List<Edge>> movieMap;
	// our movieGraphID for use in the hashCode()
	int movieGraphID;

	public MovieGraph() {
		// initialize movieMap
		movieMap = new HashMap<Movie, List<Edge>>();
		// give it an ID
		movieGraphID = 37;
	}

	/**
	 * Add a new movie to the graph. If the movie already exists in the graph
	 * then this method will return false. Otherwise this method will add the
	 * movie to the graph and return true.
	 * 
	 * @param movie
	 *            the movie to add to the graph. Requires that movie != null.
	 * @return true if the movie was successfully added and false otherwise.
	 * @modifies this by adding the movie to the graph if the movie did not
	 *           exist in the graph.
	 */
	public boolean addVertex(Movie movie) {
		// check to see if the map contains the movie passed
		if (movieMap.containsKey(movie)) {
			return false;
		}
		// create a new, empty list of our edges as our value for our key
		List<Edge> edgeList = new ArrayList<Edge>();
		// add our movie to our movie graph with our empty list value
		movieMap.put(movie, edgeList);
		return true;
	}

	/**
	 * Add a new edge to the graph. If the edge already exists in the graph then
	 * this method will return false. Otherwise this method will add the edge to
	 * the graph and return true.
	 * 
	 * @param movie1
	 *            one end of the edge being added. Requires that m1 != null.
	 * @param movie2
	 *            the other end of the edge being added. Requires that m2 !=
	 *            null. Also require that m1 is not equal to m2 because
	 *            self-loops are not meaningful in this graph.
	 * @param edgeWeight
	 *            the weight of the edge being added. Requires that edgeWeight >
	 *            0.
	 * @return true if the edge was successfully added and false otherwise.
	 * @modifies this by adding the edge to the graph if the edge did not exist
	 *           in the graph.
	 */
	public boolean addEdge(Movie movie1, Movie movie2, int edgeWeight)
			throws NoPathException {
		// create a new edge object based upon the paramaters passed to this
		// method
		Edge edge = new Edge(movie1, movie2, edgeWeight);
		if (movie1.equals(movie2)) {
			throw new NoPathException();
		}

		// check to see if the list of edges associated with the source movie
		// already contains this edge
		List<Edge> edgeList = new ArrayList<>(movieMap.get(movie1));
		for (Edge tempEdge : edgeList) {
			if (tempEdge.equals(edge)) {
				return false;
			}
		}
		edgeList.add(edge);
		// if not add edge to the list associated with the target
		movieMap.put(movie1, edgeList);
		return true;
	}

	/**
	 * Add a new edge to the graph. If the edge already exists in the graph then
	 * this method should return false. Otherwise this method should add the
	 * edge to the graph and return true.
	 * 
	 * @param movieId1
	 *            the movie id for one end of the edge being added. Requires
	 *            that m1 != null.
	 * @param movieId2
	 *            the movie id for the other end of the edge being added.
	 *            Requires that m2 != null. Also require that m1 is not equal to
	 *            m2 because self-loops are not meaningful in this graph.
	 * @param edgeWeight
	 *            the weight of the edge being added. Requires that edgeWeight >
	 *            0.
	 * @return true if the edge was successfully added and false otherwise.
	 * @throws NoSuchMovieException
	 * @throws IOException
	 * @throws NoPathException
	 * @modifies this by adding the edge to the graph if the edge did not exist
	 *           in the graph.
	 */
	public boolean addEdge(int movieId1, int movieId2, int edgeWeight)
			throws IOException, NoSuchMovieException, NoPathException {
		// get the movie object based on our ID
		Movie movie1 = getMovieObject(movieId1);
		Movie movie2 = getMovieObject(movieId2);
		// pass the two movie objects to our other addEdge method shown above
		return addEdge(movie1, movie2, edgeWeight);
	}

	/**
	 * Return the length of the shortest path between two movies in the graph.
	 * Throws an exception if the movie ids do not represent valid vertices in
	 * the graph.
	 * 
	 * @param moviedId1
	 *            the id of the movie at one end of the path.
	 * @param moviedId2
	 *            the id of the movie at the other end of the path.
	 * @throws NoSuchMovieException
	 *             if one or both arguments are not vertices in the graph.
	 * @throws NoPathException
	 *             if there is no path between the two vertices in the graph.
	 * 
	 * @return the length of the shortest path between the two movies
	 *         represented by their movie ids.
	 * @throws IOException
	 */
	public int getShortestPathLength(int movieId1, int movieId2)
			throws NoSuchMovieException, NoPathException, IOException {
		
		//check for equality
		if(movieId1 == movieId2){
			throw new NoPathException();
		}
		
		List<Movie> pathList = new ArrayList<Movie>();
		Movie sourceMovie = getMovieObject(movieId1);
		Movie targetMovie = getMovieObject(movieId2);
		int distV = Integer.MAX_VALUE;
		int distU = 0;
		int distU2 = 0;
		int alt = 0;
		int targetDistance = 0;
		int travelDistance = Integer.MAX_VALUE;
				
		//add to do not touch list
		pathList.add(sourceMovie);
		
		//check to see if we are at our target
		while (sourceMovie.hashCode() != targetMovie.hashCode()) {
			
			//new list to contain edges for our source
			List<Edge> edgeList = new ArrayList<>(movieMap.get(sourceMovie));
			
			//iterate through edges
			for (Edge tempEdge : edgeList) {
				//check to see if any edge targets equal our target (should be at least one for every movie)
				if(tempEdge.getTargetMovie().hashCode() == targetMovie.hashCode()){
					
					//calculate new total distance
					targetDistance = distU + tempEdge.getEdgeWeight();
					
					if(targetDistance < travelDistance){
						//preserve the SMALLEST total distance for edges that point to our target
						travelDistance = targetDistance;
					}
					
				}
				//if movie isnt on our do not touch list
				if (!(pathList.contains(tempEdge.getTargetMovie()))) {
				
					//this chunk of code determines a movies edge which has the smallest edge weight and where it points
					alt = distU + tempEdge.getEdgeWeight();
					
					if (alt < distV) {
						distV = alt;
						//preserve the target with the shortest total distance and update source movie to that
						sourceMovie = tempEdge.getTargetMovie();
						distU2 = distU + distV;
					}		
				}
			}
			//update total distance traveled, make distV back to MAX_VALUE to restart comparisons
			distU = distU2;
			distV = Integer.MAX_VALUE;
			//add new source to do not touch list
			pathList.add(sourceMovie);
		}
		
		return travelDistance;
	}

	/**
	 * Return the shortest path between two movies in the graph. Throws an
	 * exception if the movie ids do not represent valid vertices in the graph.
	 * 
	 * @param moviedId1
	 *            the id of the movie at one end of the path.
	 * @param moviedId2
	 *            the id of the movie at the other end of the path.
	 * @throws NoSuchMovieException
	 *             if one or both arguments are not vertices in the graph.
	 * @throws NoPathException
	 *             if there is no path between the two vertices in the graph.
	 * 
	 * @return the shortest path, as a list, between the two movies represented
	 *         by their movie ids. This path begins at the movie represented by
	 *         movieId1 and ends with the movie represented by movieId2.
	 * @throws IOException
	 */
	public List<Movie> getShortestPath(int movieId1, int movieId2)
			throws NoSuchMovieException, NoPathException, IOException {
		
		if(movieId1 == movieId2){
			throw new NoPathException();
		}
		
		List<Movie> pathList = new ArrayList<Movie>();
		List<Movie> returnList = new ArrayList<Movie>();
		Movie sourceMovie = getMovieObject(movieId1);
		Movie targetMovie = getMovieObject(movieId2);
		int distV = Integer.MAX_VALUE;
		int distU = 0;
		int distU2 = 0;
		int alt = 0;
		int targetDistance = 0;
		int travelDistance = Integer.MAX_VALUE;
		
		//add to do not touch list
		pathList.add(sourceMovie);
		//add to list we want to return
		returnList.add(sourceMovie);
		
		//check to see if we are at our target
		while (sourceMovie.hashCode() != targetMovie.hashCode()) {
			
			List<Edge> edgeList = new ArrayList<>(movieMap.get(sourceMovie));
			
			//iterate through edges
			for (Edge tempEdge : edgeList) {
				
				if(tempEdge.getSourceMovie().hashCode() == targetMovie.hashCode()){
					
					targetDistance = distU + tempEdge.getEdgeWeight();
					
					if(targetDistance < travelDistance){
						//similar to our other algorithm method, except in this case we also preserve our current do not touch list
						travelDistance = targetDistance;
						returnList =pathList;
					}
					
					
				}
				//same as the other the implementation in the other method, used to get smallest edge weight
				if (!(pathList.contains(tempEdge.getTargetMovie()))) {
				
					alt = distU + tempEdge.getEdgeWeight();
					
					if (alt < distV) {
						distV = alt;
						sourceMovie = tempEdge.getTargetMovie();
						distU2 = distU + distV;
					}		
				}
			}
			distU = distU2;
			distV = Integer.MAX_VALUE;
			pathList.add(sourceMovie);
		}
		//add target to return list
		returnList.add(targetMovie);
		
		return returnList;
	}
				

	/**
	 * Returns the movie id given the name of the movie. For movies that are not
	 * in English, the name contains the English transliteration original name
	 * and the English translation. A match is found if any one of the two
	 * variations is provided as input. Typically the name string has <English
	 * Translation> (<English Transliteration>) for movies that are not in
	 * English.
	 * 
	 * @param name
	 *            the movie name for the movie whose id is needed.
	 * @return the id for the movie corresponding to the name. If an exact match
	 *         is not found then return the id for the movie with the best match
	 *         in terms of translation/transliteration of the movie name.
	 * @throws NoSuchMovieException
	 *             if the name does not match any movie in the graph.
	 * @throws IOException
	 */
	public int getMovieId(String name) throws NoSuchMovieException, IOException {
		// checks to see if the movie with the associated name is null
		if (getMovieObject(name) == null) {
			throw new NoSuchMovieException();
		}
		// returns the ID of the movie associated with the name
		return getMovieObject(name).hashCode();
	}

	/**
	 * Returns a movie given the movie's unique ID
	 * 
	 * @param id
	 *            : the movie id for the movie which is needed.
	 * @return the movie with the associated ID
	 * @throws NoSuchMovieException
	 *             if the id does not match any movie in the graph.
	 * @throws IOException
	 */
	public Movie getMovieObject(int id) throws IOException,
			NoSuchMovieException {
		Set<Movie> movieSet = new HashSet<Movie>(movieMap.keySet());
		for (Movie tempMovie : movieSet) {
			if (tempMovie.hashCode() == id) {
				return tempMovie;
			}
		}
		// if there was no matched movie, throw exception
		throw new NoSuchMovieException();
	}

	/**
	 * Returns the movie which "best matches" the given name
	 * 
	 * @param name
	 *            : the movie name you wish the search the dataset for.
	 * @return the movie that best matches the given search parameter
	 * @throws NoSuchMovieException
	 *             if the name does not match any movie in the graph.
	 * @throws IOException
	 */
	public Movie getMovieObject(String name) throws IOException,
			NoSuchMovieException {
		Set<Movie> movieSet = new HashSet<Movie>(movieMap.keySet());
		// put the name into a string buffer so we can preform operations on it
		StringBuffer nameBuffer = new StringBuffer(name);
		// get initial nameBuffer length
		int n = nameBuffer.length();
		for (int i = 0; i < n; i++) {
			for (Movie tempMovie : movieSet) {
				if (tempMovie.getName().contains(nameBuffer)) {
					return tempMovie;
				}
			}
			// delete the greatest index and repeat the search
			nameBuffer.deleteCharAt(nameBuffer.length() - 1);
		}
		throw new NoSuchMovieException();
	}

	private Map<Movie, List<Edge>> getMap() {
		return movieMap;
	}

	@Override
	public boolean equals(Object other) {
		if ((other instanceof MovieGraph)
				&& (this instanceof MovieGraph)
				&& (((MovieGraph) other).getMap().size() == (this.getMap()
						.size()))) {
			Set<Movie> thisKeys = this.getMap().keySet();
			Set<Movie> otherKeys = ((MovieGraph) other).getMap().keySet();
			return otherKeys.containsAll(thisKeys);
		}
		return false;
	}

	@Override
	public int hashCode() {
		if (this instanceof MovieGraph) {
			return movieGraphID * this.getMap().size();
		}
		return movieGraphID;
	}
}