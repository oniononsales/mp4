package mp4;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
public class MovieGraphMain {
	public static void main(String[] args) throws IOException, NoPathException {
		// creates new movieGraph
		MovieGraph movieGraph = new MovieGraph();
		// adds all ratings from data set to set of ratings
		RatingIterator rIterator = new RatingIterator("data\\u.data.txt");
		Set<Rating> ratingSet = new HashSet<Rating>();
		while (rIterator.hasNext() == true) {
			ratingSet.add(rIterator.getNext());
		}
		// creates new movieIterator
		MovieIterator sourceIterator = new MovieIterator("data\\u.item.txt");
		while (sourceIterator.hasNext()) {
			// get the next movie from the source iterator
			Movie sourceMovie = sourceIterator.getNext();
			// add source movie to the graph
			movieGraph.addVertex(sourceMovie);
			// creates a new iterator for target movies
			MovieIterator targetIterator = new MovieIterator("data\\u.item.txt");
			// checks to see if there is another movie in the set
			while (targetIterator.hasNext()) {
				// gets the next movie
				Movie targetMovie = targetIterator.getNext();
				// if not same movie, get edge weights
				if (!(sourceMovie.equals(targetMovie))) {
					// two sets of ratings, one for source and one for target
					Set<Rating> sourceRatings = new HashSet<Rating>();
					Set<Rating> targetRatings = new HashSet<Rating>();
					// iterator through the rating set, searching for all
					// ratings related to source and target
					for (Rating tempRating : ratingSet) {
						if (sourceMovie.hashCode() == tempRating.getMovieId()) {
							sourceRatings.add(tempRating);
						}
						if (targetMovie.hashCode() == tempRating.getMovieId()) {
							targetRatings.add(tempRating);
						}
					}
					// get the edge weight between the source and target movie
					int edgeWeight = getEdgeWeight(sourceRatings, targetRatings);
					System.out.println(sourceMovie.getName() + " " + edgeWeight + " " + targetMovie.getName());
					// add a new edge to the movie graph for the source movie
					movieGraph.addEdge(sourceMovie, targetMovie, edgeWeight);
				}
				
			}

		}
		
	}
	private static int getEdgeWeight(Set<Rating> sourceSet,
			Set<Rating> targetSet) {
		// initialize new integers for our operations
		int likerUnionSize = 0;
		int dislikerUnionSize = 0;
		int uniqueReviewers = 0;
		// two enhanced for loops to compare every rating in our source set to
		// every rating in our target set
		for (Rating tempSourceRating : sourceSet) {
			for (Rating tempTargetRating : targetSet) {
				// if users are the same
				if (tempSourceRating.getUserId() == tempTargetRating
						.getUserId()) {
					// if both ratings >3, add to likerUnionSize
					if (tempSourceRating.getRating() > 3
							&& tempTargetRating.getRating() > 3) {
						likerUnionSize++;
					}
					// if both ratings ,add to disliker union size
					if (tempSourceRating.getRating() < 3
							&& tempTargetRating.getRating() < 3) {
						dislikerUnionSize++;
					}
				}
				// if reviewers are unique, add to unique reviewer size
				if (tempTargetRating.getUserId() != tempSourceRating
						.getUserId()) {
					uniqueReviewers++;
				}
			}
		}
		// ASSUMPTION: number of reviewers implies UNIQUE REVIEWERS between two sets!
		return 1 + uniqueReviewers - (likerUnionSize + dislikerUnionSize);
	}
}
