package mp4;

import static org.junit.Assert.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MovieGraphTest {
	MovieGraph graph;
	MovieGraph replicateGraph;
	MovieGraph unknownObject;
	Object realObject;
	Movie movie1;
	Movie movie2;
	Movie movie3;
	Movie movie4;
	Movie movie5;
	Movie movie6;
	int edgeWeight;
	Edge edge;

	@Before
	public void setup() throws IOException, NoSuchMovieException,
			NoPathException {
		// initializes the object
		realObject = new Movie(365, "Dude", 2014, "www.facebook.com");
		// constructs two new MovieGraphs
		graph = new MovieGraph();
		unknownObject = new MovieGraph();
		// constructs six new Movies
		movie1 = new Movie(1, "Toy Story", 1995,
				"http://us.imdb.com/M/title-exact?Toy%20Story%20");
		movie2 = new Movie(1093, "Live Nude Girls", 1995,
				"http://us.imdb.com/M/title-exact?Live%20Nude%20Girls%20");
		movie3 = new Movie(8, "Babe", 1995,
				"http://us.imdb.com/M/title-exact?Babe%20");
		movie4 = new Movie(1360, "Sexual Life of the Belgians, The", 1994,
				"http://us.imdb.com/M/title-exact?Vie%20sexuelle%20des%20Belges,%20La%20");
		movie5 = new Movie(
				1366,
				"JLG/JLG - autoportrait de d�cembre",
				1994,
				"http://us.imdb.com/M/title-exact?JLG/JLG%20-%20autoportrait%20de%20d%E9cembre%20%281994%29");
		movie6 = new Movie(2, "GoldenEye", 1995,
				"http://us.imdb.com/M/title-exact?GoldenEye%20");
		// set the edgeWeight as an arbitrary value
		edgeWeight = 37;
		// constructs an Edge between movie1 and movie2
		edge = new Edge(movie1, movie2, edgeWeight);
		// adds the vertex of movie1, 2, 3 to the graph
		graph.addVertex(movie1);
		graph.addVertex(movie2);
		graph.addVertex(movie3);
		graph.addVertex(movie4);
		// adds all the edges between movie1, 2, 3
		graph.addEdge(movie1, movie2, edgeWeight);
		graph.addEdge(movie1, movie3, edgeWeight);
		graph.addEdge(movie2, movie1, edgeWeight);
		graph.addEdge(movie2, movie3, edgeWeight);
		graph.addEdge(movie3, movie1, edgeWeight);
		graph.addEdge(movie3, movie2, edgeWeight);
		graph.addEdge(movie1, movie4, 200);
		graph.addEdge(movie4, movie1, 200);
		graph.addEdge(movie2, movie4, 20);
		graph.addEdge(movie4, movie2, 20);
		graph.addEdge(movie3, movie4, 30);
		graph.addEdge(movie4, movie3, 30);

		replicateGraph = graph;
	}

	@Test
	public void testAddVertex() {
		// asserts that a movie that has already been added as a vertex
		// should not be added again, returning false
		assertFalse(graph.addVertex(movie1));
		// asserts that a movie that has not been added can be added and adds
		// to graph, returning true
		assertTrue(graph.addVertex(movie5));
	}

	@Test
	public void testAddEdgeMovie() throws NoPathException {
		// asserts that the edge to be added has already been added, returning
		// false
		assertFalse(graph.addEdge(movie1, movie2, edgeWeight));
		assertFalse(graph.addEdge(movie1, movie4, edgeWeight));
		// asserts that the method cannot add the edge of two same movies,
		// throwing a no path exception
		try {
			graph.addEdge(movie1, movie1, edgeWeight);
			fail("There should have been an exception!");
		} catch (NoPathException e) {
			// we should get an exception so the assert should be fine
			assertTrue(true);
		}
	}

	@Test
	public void testAddEdge() throws IOException, NoSuchMovieException,
			NoPathException {
		// same tests as testAddEdgeMovie except we are using MovieIds
		// asserts that the edge to be added has already been added, returning
		// false
		assertFalse(graph.addEdge(movie1.hashCode(), movie2.hashCode(),
				edgeWeight));
		// asserts that the edge to be added has not been added, returning true
		assertFalse(graph.addEdge(movie1.hashCode(), movie4.hashCode(),
				edgeWeight));
		// asserts that the method cannot add the edge of two same movies,
		// throwing a
		// no path exception
		try {
			graph.addEdge(movie1.hashCode(), movie1.hashCode(), edgeWeight);
			fail("There should have been an exception!");
		} catch (NoPathException e) {
			// we should get an exception so the assert should be fine
			assertTrue(true);
		}
	}

	@Test
	public void testGetShortestPathLength() throws NoSuchMovieException,
			NoPathException, IOException {
		
		
		// asserts that the shortest path length from movie1 to movie2 is the
		// same vice versa
		assertTrue(graph.getShortestPathLength(movie1.hashCode(),
				movie2.hashCode()) == graph.getShortestPathLength(
				movie2.hashCode(), movie1.hashCode()));
		
	}

	@Test
	public void testGetShortestPathVertics() throws NoSuchMovieException,
			NoPathException, IOException {
		
		
		
		
		// assumes that the path taken from movie1 to movie2 is the same as
		// movie2 to movie1
		// first we check that the length of the lists produced in opposite
		// directions are the same
		assertEquals(graph
				.getShortestPath(movie1.hashCode(), movie2.hashCode()).size(),
				graph.getShortestPath(movie1.hashCode(), movie2.hashCode())
						.size());
		// now we iterate through the vertex of one list and checks that the
		// other list will also contain
		// the same vertex
		for (int i = 0; i < graph.getShortestPath(movie1.hashCode(),
				movie2.hashCode()).size(); i++) {
			assertTrue(graph.getShortestPath(movie1.hashCode(),
					movie2.hashCode()).contains(
					graph.getShortestPath(movie2.hashCode(), movie1.hashCode())
							.get(i)));
		}
		// asserts that path taken from a set of movies is the not same as the
		// path from another set
		assertFalse(graph.getShortestPath(movie1.hashCode(), movie2.hashCode())
				.contains(
						graph.getShortestPath(movie2.hashCode(),
								movie3.hashCode())));
		assertFalse(graph.getShortestPath(movie2.hashCode(), movie3.hashCode())
				.contains(
						graph.getShortestPath(movie3.hashCode(),
								movie1.hashCode())));
	}

	@Test
	public void testGetMovieId() throws NoSuchMovieException, IOException {
		// asserts that one can obtain the movie id by having its respective
		// movie name
		// as long as the the movie is in the graph
		assertEquals(movie1.hashCode(), graph.getMovieId(movie1.getName()));
		assertEquals(movie2.hashCode(), graph.getMovieId(movie2.getName()));
	}

	@Test
	public void testGetFromId() throws IOException, NoSuchMovieException {
		// asserts that one can obtain the Movie itself by having its respective
		// movie
		// id as long as the movie is in the graph
		assertEquals(movie1, graph.getMovieObject(movie1.hashCode()));
		assertEquals(movie2, graph.getMovieObject(movie2.hashCode()));
	}

	@Test
	public void testGetFromName() throws IOException, NoSuchMovieException {
		// asserts that one can obtain the Movie itself by having its respective
		// movie
		// name as long as the movie is in the graph
		assertEquals(movie1, graph.getMovieObject(movie1.getName()));
		assertEquals(movie2, graph.getMovieObject(movie2.getName()));
	}

	@Test
	public void testEquals() {
		assertTrue(graph.equals(replicateGraph));
		assertTrue(replicateGraph.equals(graph));
		// even though unknownObject is an instance of movie graph, it is not
		// the same graph
		assertFalse(unknownObject.equals(graph));
		// an object that is not an instance of movie graph is not equal to
		// graph
		assertFalse(realObject.equals(graph));
	}

	@Test
	public void testHashCode() {
		assertTrue(graph.hashCode() == replicateGraph.hashCode());
		assertFalse(graph.hashCode() == unknownObject.hashCode());
		assertFalse(graph.hashCode() == realObject.hashCode());
	}
	
	@Test
	public void comprehensiveTest() throws NoSuchMovieException, NoPathException, IOException{
		
		List<Movie> vertex12 = new ArrayList<>(graph.getShortestPath(movie1.hashCode(), movie2.hashCode()));
		System.out.println(vertex12.toString());
		List<Movie> vertex21 = new ArrayList<>(graph.getShortestPath(movie2.hashCode(), movie1.hashCode()));
		System.out.println(vertex21.toString());
		List<Movie> vertex23 = new ArrayList<>(graph.getShortestPath(movie2.hashCode(), movie3.hashCode()));
		List<Movie> vertex32 = new ArrayList<>(graph.getShortestPath(movie3.hashCode(), movie2.hashCode()));
		List<Movie> vertex13 = new ArrayList<>(graph.getShortestPath(movie1.hashCode(), movie3.hashCode()));
		List<Movie> vertex31 = new ArrayList<>(graph.getShortestPath(movie3.hashCode(), movie1.hashCode()));
		for(int a = 0; a < vertex12.size(); a++) {
			System.out.println("Vertex12: " + vertex12.get(a).getName());
		}
		for(int a = 0; a < vertex21.size(); a++) {
			System.out.println("Vertex21: " + vertex21.get(a).getName());
		}
		for(int a = 0; a < vertex23.size(); a++) {
			System.out.println("Vertex23: " + vertex23.get(a).getName());
		}
		for(int a = 0; a < vertex32.size(); a++) {
			System.out.println("Vertex32: " + vertex32.get(a).getName());
		}
		for(int a = 0; a < vertex13.size(); a++) {
			System.out.println("Vertex13: " + vertex13.get(a).getName());
		}
		for(int a = 0; a < vertex31.size(); a++) {
			System.out.println("Vertex31: " + vertex31.get(a).getName());
		}
		
		// prints out the shortest distance for convenience of checking
				System.out.println("Shortest path from 1 to 2 is: " + graph.getShortestPathLength(movie1.hashCode(), movie2.hashCode()));
				System.out.println("Shortest path from 2 to 1 is: " + graph.getShortestPathLength(movie2.hashCode(), movie1.hashCode()));
				System.out.println("Shortest path from 2 to 3 is: " + graph.getShortestPathLength(movie2.hashCode(), movie3.hashCode()));
				System.out.println("Shortest path from 3 to 2 is: " + graph.getShortestPathLength(movie3.hashCode(), movie2.hashCode()));
				System.out.println("Shortest path from 1 to 3 is: " + graph.getShortestPathLength(movie1.hashCode(), movie3.hashCode()));
				System.out.println("Shortest path from 3 to 1 is: " + graph.getShortestPathLength(movie3.hashCode(), movie1.hashCode()));
				System.out.println("Shortest path from 1 to 4 is: " + graph.getShortestPathLength(movie1.hashCode(), movie4.hashCode()));
				System.out.println("Shortest path from 4 to 1 is: " + graph.getShortestPathLength(movie4.hashCode(), movie1.hashCode()));
				// below should produce 0
		
	}
}