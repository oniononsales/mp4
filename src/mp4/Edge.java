package mp4;

public class Edge {
	private final Movie sourceMovie;
	private final Movie targetMovie;
	private final int edgeWeight;

	public Edge(Movie sourceMovie, Movie targetMovie, int edgeWeight) {
		this.sourceMovie = sourceMovie;
		this.targetMovie = targetMovie;
		this.edgeWeight = edgeWeight;
	}

	public Movie getSourceMovie() {
		return sourceMovie;
	}

	public Movie getTargetMovie() {
		return targetMovie;
	}

	public int getEdgeWeight() {
		return edgeWeight;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Edge && this instanceof Edge) {

			if (this.getSourceMovie().hashCode() != ((Edge) other)
					.getSourceMovie().hashCode()) {
				return false;
			}
			if (this.getTargetMovie().hashCode() != ((Edge) other)
					.getTargetMovie().hashCode()) {
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		if (this instanceof Edge) {
			return 37 * this.getEdgeWeight();
		}
		return 37;
	}
}